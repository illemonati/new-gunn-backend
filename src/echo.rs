use actix_web::{get, web, Responder};
use serde::Serialize;

#[derive(Serialize)]
struct EchoMessage {
    message: String,
}

#[get("/api/echo/{message}")]
pub async fn echo(info: web::Path<(String,)>) -> impl Responder {
    let res = EchoMessage {
        message: info.0.clone(),
    };
    web::Json(res)
}
