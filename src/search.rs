use actix_web::{get, web, Responder};
use serde::Serialize;
use std::any::Any;
use std::convert::TryInto;
use std::error::Error;
use std::path::Path;
use std::sync::mpsc::sync_channel;
use tantivy::collector::TopDocs;
use tantivy::directory::MmapDirectory;
use tantivy::query::QueryParser;
use tantivy::schema::{Field, FieldType, Schema};
use tantivy::tokenizer::{
    AlphaNumOnlyFilter, Language, LowerCaser, RemoveLongFilter, SimpleTokenizer, Stemmer, Tokenizer,
};
use tantivy::{Index, IndexReader, ReloadPolicy, TantivyError};

use lazy_static;
lazy_static! {
    static ref SEARCH_PROVIDER: SearchProvider =
        SearchProvider::new("./index/new-gunn-index").unwrap();
}

#[derive(Serialize, Debug, Clone)]
struct SearchResults {
    hits: Vec<SearchResult>,
}
#[derive(Serialize, Debug, Clone)]
struct SearchResult {
    title: String,
    url: String,
}

struct SearchProvider {
    schema: Schema,
    reader: IndexReader,
    query_parser: QueryParser,
}

impl SearchProvider {
    fn new(path: impl Into<String>) -> Result<SearchProvider, TantivyError> {
        let index = Index::open_in_dir(path.into())?;
        index.tokenizers().register(
            "commoncrawl",
            SimpleTokenizer
                .filter(RemoveLongFilter::limit(40))
                .filter(LowerCaser)
                .filter(AlphaNumOnlyFilter)
                .filter(Stemmer::new(Language::English)),
        );
        let schema = index.schema();
        let title = schema.get_field("title").unwrap();
        let body = schema.get_field("body").unwrap();
        let query_parser = QueryParser::new(
            schema.clone(),
            vec![title, body],
            index.tokenizers().clone(),
        );
        let reader = index
            .reader_builder()
            .reload_policy(ReloadPolicy::OnCommit)
            .try_into()?;

        Ok(SearchProvider {
            schema,
            query_parser,
            reader,
        })
    }

    fn search(
        &self,
        query: impl Into<String>,
        limit: usize,
    ) -> Result<SearchResults, TantivyError> {
        let searcher = self.reader.searcher();
        let query = self.query_parser.parse_query(&query.into())?;
        let results = searcher.search(&query, &TopDocs::with_limit(limit))?;
        let res: Vec<SearchResult> = results
            .iter()
            .map(|(_, docaddress)| {
                let retrieved = searcher.doc(*docaddress).unwrap();
                let title = retrieved
                    .get_first(self.schema.get_field("title").unwrap())
                    .unwrap();
                let url = retrieved
                    .get_first(self.schema.get_field("url").unwrap())
                    .unwrap();
                SearchResult {
                    title: title.text().unwrap().to_string(),
                    url: url.text().unwrap().to_string(),
                }
            })
            .collect();
        Ok(SearchResults { hits: res })
    }
}


#[get("/api/search/{search_terms}")]
pub async fn search(info: web::Path<(String,)>) -> impl Responder {
    let search_terms = &info.0;
    let res = SEARCH_PROVIDER.search(search_terms, 5).unwrap();
    web::Json(res)
}
