#[macro_use]
extern crate lazy_static;

use actix_web::{App, HttpServer};
mod echo;
mod search;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().service(echo::echo).service(search::search))
        .bind("127.0.0.1:8080")?
        .run()
        .await
}